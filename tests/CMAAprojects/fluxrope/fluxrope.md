# Test fluxrope

\test fluxrope
\todo Describe what this test does
\todo Test does not compile:

    amrvacusr.f:293.13:
    if(energyonly) return
             1
    Error: Symbol 'energyonly' at (1) has no IMPLICIT type

# Setup instructions

Setup this test case with

    $AMRVAC_DIR/setup.pl -d=23 -phi=0 -z=0 -g=14,14 -p=mhd

# Description

This test has no description yet


