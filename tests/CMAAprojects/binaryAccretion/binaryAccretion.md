# Test binaryAccretion

\test binaryAccretion
\todo Describe what this test does
\todo Test does not compile:

    amr_solution_node.f:127.30:
    phyboundblock(igrid)=.true.
                              1
    Error: Unexpected STATEMENT FUNCTION statement at (1)

# Setup instructions

Setup this test case with

    $AMRVAC_DIR/setup.pl -d=33 -phi=0 -z=0 -g=10,10,10 -p=hd -eos=default -nf=1 -ndust=0 -u=nul

# Description

This test has no description yet.


